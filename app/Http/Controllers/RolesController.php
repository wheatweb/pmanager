<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::check()) {
            $roles = Role::all();
            
            return view('roles.index', ['roles'=> $roles]);
        }
        
        return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(Auth::check()) {
            $role = Role::create(['name'=> $request->input('name')]);
            
            if($role) {
                return redirect()->route('roles.show', ['role'=> $role->id])->with('success', $role->name.' role was created successfully.');
           }
        }
        
        return back()->withInput()->with('error', 'The new role '.$role->name.' was not created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
        $role = Role::find($role->id);
        
        return view('roles.show', ['role'=> $role]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //
        $role = Role::find($role->id);
        
        view('role.edit', ['role'=> $role]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        //
        $roleUpdated = Role::where('id', $role->id)->update(['name'=> $request->input('name')]);
        
        if($roleUpdated) {
            return redirect()->route('routes.show', ['role'=> $role->id])->with('success', $role->name.' was updated successfully.');
        }
        
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
        $role = Role::find($role->id);
        if($role->delete()) {
            return redirect()->route('role.index')->with('success', 'The '.$role-name.' was removed successfully.');
        }
        
        return back()->withInput()->with('error', $role-name.' could not be deleted.');
    }
}
