@extends('layouts.app')

@section('content')
<div class="col-md-12 col-lg-12">
    <div class="card">
        <div class="card-header bg-primary text-white"><h4>Roles</h4></div>
        <div class="card-body">
            <ul class="list-group">
                <li class="list-group-flush list-group-item"><a href="/roles/create/" class="float-right btn btn-primary btn-sm">Add a Role</a></li>
            @foreach($roles as $role)
                <li class="list-group-item"><a href="/roles/{{ $role->id }}">{{ $role->name }}</a></li>
            @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection