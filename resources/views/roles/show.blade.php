@extends('layouts.app')

@section('content')
<div class="col-lg-9 col-md-9 col-sm-9 pull-left">
    <div class="jumbotron">
        <h1 class="display-3">{{ $role->name }}</h1>
    </div>

    <!-- Example row of columns -->
    <div class="row col-lg-12 col-md-12 col-sm-12">
        <a href="/roles/create/{{ $role->id }}" class="float-right btn btn-primary btn-sm">Add Role</a>
    </div>
</div>
        
<aside class="col-lg-3 col-md-3 col-sm-3 pull-right">
    <div class="sidebar-module">
        @include('partials.view-nav')
        
        <h4>Actions</h4>
        <ol class="list-unstyled">
            <li><a href="/roles/{{ $role->id }}/edit/">Edit {{ $role->name }}</a></li>
            <li><a href="#" onclick="
                                    var result = confirm('Are you sure you wish to delete {{ $role->name }}?');
                                    if(result) {
                                        event.preventDefault();
                                        document.getElementById('delete-form').submit();
                                    }
                                    ">
                    Delete {{ $role->name }}
                </a>
                <form id="delete-form" action="{{ route('roles.destroy', [$role->id]) }}" method="POST" style="display: none;">
                    @csrf_field
                    <input type="hidden" name="_method" value="delete" />
                </form>
            </li>
        </ol>
    </div>
</aside>
<hr>
@endsection