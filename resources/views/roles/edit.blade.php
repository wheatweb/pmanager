@extends('layouts.app')

@section('content')
<div class="col-lg-9 col-md-9 col-sm-9 pull-left">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <form method="post" action="{{ route('route.update', [$route->id]) }}">
                @csrf
                <input type="hidden" name="_method" value="put" />
                <div class="form-group">
                    <label for="company-name">Name<span class="required">*</span></label>
                    <input placeholder="Enter Name" id="role-name" required name="name" spellcheck="false" class="form-control" value="{{ $role->name }}" />
                </div>
                
                <div class="form-group"><input type="submit" class="btn btn-primary" value="Submit" /></div>
            </form>
        </div>
    </div>
    <hr>
</div>

<aside class="col-lg-3 col-md-3 col-sm-3 blog-sidebar pull-right">
    <div class="sidebar-module">
        @include('partials.view-nav');
    </div>
</aside>
@endsection