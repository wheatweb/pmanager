@extends('layouts.app')

@section('content')
<div class="col-md-12 col-lg-12">
    <div class="card">
        <div class="card-header bg-primary text-white"><h4>Companies</h4></div>
        <div class="card-body">
            <ul class="list-group">
                <li class="list-group-flush list-group-item"><a href="/companies/create/" class="float-right btn btn-primary btn-sm">Add a Company</a></li>
            @foreach($companies as $company)
                <li class="list-group-item"><a href="/companies/{{ $company->id }}">{{ $company->name }}</a></li>
            @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection