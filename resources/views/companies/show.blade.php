@extends('layouts.app')

@section('content')
<div class="col-lg-9 col-md-9 col-sm-9 pull-left">
    <div class="jumbotron">
        <h1 class="display-3">{{ $company->name }}</h1>
        <p>{{ $company->description }}</p>
        <!-- <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more »</a></p> -->
    </div>

    <!-- Example row of columns -->
    <div class="row col-lg-12 col-md-12 col-sm-12">
        <a href="/projects/create/{{ $company->id }}" class="float-right btn btn-primary btn-sm">Add Project</a>
    </div>
        
    <div class="row col-lg-12 col-md-12 col-sm-12">
    @foreach($company->projects as $project)
        <div class="col-lg-4 col-md-4 col-sm-4">
            <h2>{{ $project->name }}</h2>
            <p>{{ $project->description }}</p>
            <p><a class="btn btn-secondary" href="/projects/{{ $project->id }}" role="button">View Project »</a></p>
        </div>
    @endforeach
    </div>
</div>
        
<aside class="col-lg-3 col-md-3 col-sm-3 pull-right">
    <div class="sidebar-module">
        <h4>Actions</h4>
        <ol class="list-unstyled">
            <li><a href="/companies/">My Companies</a></li>
            <br />
            <li><a href="/companies/{{ $company->id }}/edit/">Edit {{ $company->name }}</a></li>
            <li><a href="/projects/create/{{ $company->id }}">Add Project</a></li>
            <br />
            <li><a href="#" onclick="
                                    var result = confirm('Are you sure you wish to delete {{ $company->name }}?');
                                    if(result) {
                                        event.preventDefault();
                                        document.getElementById('delete-form').submit();
                                    }
                                    ">
                    Delete {{ $company->name }}
                </a>
                <form id="delete-form" action="{{ route('companies.destroy', [$company->id]) }}" method="POST" style="display: none;">
                    <input type="hidden" name="_method" value="delete" />
                    {{ csrf_field() }}
                </form>
            </li>
        </ol>
    </div>
        
    <!--
    <div class="sidebar-module">
        <h4>Members</h4>
        <ol class="list-unstyled">
            <li><a href=""></a></li>
        </ol>
    </div>
    -->
</aside>
<hr>
@endsection