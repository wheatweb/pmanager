@extends('layouts.app')

@section('content')
<div class="col-lg-9 col-md-9 col-sm-9 pull-left">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <h4>Edit Project</h4>
            <form method="post" action="{{ route('projects.update', [$project->id]) }}">
                @csrf
                <input type="hidden" name="_method" value="put" />
                <div class="form-group">
                    <label for="project-name">Name<span class="required">*</span></label>
                    <input placeholder="Enter Name" id="project-name" required name="name" spellcheck="false" class="form-control" value="{{ $project->name }}" />
                </div>
                
                <div class="form-group">
                    <label for="company-name">Company</label>
                    <input disabled id="company-name" name="company-name" spellcheck="false" class="form-control" value="{{ $company->name }}" />
                </div>
                
                <div class="form-group">
                    <label for="project-description">Description</label>
                    <textarea placeholder="Enter Description" id="project-description" name="description" rows="5" spellcheck="false" class="form-control autosize-target text-left">
                        {{ $project->description }}
                    </textarea>
                </div>
                
                <div class="form-group">
                    <label for="project-days">Days</label>
                    <input placeholder="Time to complete" id="project-days" required name="days" spellcheck="false" class="form-control" value="{{ $project->days }}" />
                </div>
                    
                <div class="form-group"><input type="submit" class="btn btn-primary" value="Submit" /></div>
            </form>
        </div>
    </div>
    <hr>
</div>

<aside class="col-lg-3 col-md-3 col-sm-3 blog-sidebar pull-right">
    <div class="sidebar-module">
        <h4>View</h4>
        <ol class="list-unstyled">
          <li><a href="/projects/{{ $project->id }}/"><i class="fas fa-thumbtack"></i> {{ $project->name }}</li>
          <li><a href="/projects/"><i class="fas fa-briefcase"></i> All Projects</a></li>
        </ol>
    </div>

    <!--
    <div class="sidebar-module">
        <h4>Members</h4>
        <ol class="list-unstyled">
            <li><a href=""></a></li>
        </ol>
    </div>
    -->
</aside>
@endsection