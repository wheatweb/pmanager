@extends('layouts.app')

@section('content')
<div class="col-md-12 col-lg-12">
    <div class="card">
        <div class="card-header bg-primary text-white"><h4>Projects</h4></div>
        <div class="card-body">
            <div class="row mb-3">
                <div class="col-12 themed-grid-col"><a href="/projects/create/" class="float-right btn btn-primary btn-sm">Add a Project</a></div>
                <div class="col-4">Project</div>
                <div class="col-4">Company</div>
                <div class="col-4">Days</div>
            @foreach($projects as $project)
                <div class="col-4 themed-grid-col"><a href="/projects/{{ $project->id }}">{{ $project->name }}</a></div>
                <div class="col-4 themed-grid-col">{{--<a href="/companies/{{ $company->id }}">{{ $company->name }}</a>--}}</div>
                <div class="col-4 themed-grid-col">{{ $project->days }}</div>
            @endforeach
            </div>
        </div>
    </div>
</div>
@endsection