@extends('layouts.app')

@section('content')
<div class="col-lg-9 col-md-9 col-sm-9 pull-left">
    <h4>Create a new Project</h4>
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <form method="post" action="{{ route('projects.store') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="project-name">Name<span class="required">*</span></label>
                    <input placeholder="Enter Name" id="project-name" required name="name" spellcheck="false" class="form-control" />
                </div>
                
                <div class="form_group">
                    <label for="company-id">Company<span class="required">*</span></label>
                    <select id="company-id" name="company_id" class="form-control" required>
                        <option value="">Select a Company</option>
                    @foreach($companies as $company)
                        @if ($company->id == $company_id) 
                        <option value="{{ $company->id }}" selected>{{ $company->name }}</option>
                        @else
                        <option value="{{ $company->id }}">{{ $company->name }} {{ $company->id }}</option>
                        @endif
                    @endforeach
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="project-description">Description</label>
                    <textarea placeholder="Enter Description" id="project-description" name="description" rows="5" spellcheck="false" class="form-control autosize-target text-left">
                    </textarea>
                </div>
                
                <div class="form-group">
                    <label for="project-days">Days</label>
                    <input placeholder="Time to complete" id="project-days" required name="days" spellcheck="false" class="form-control" />
                </div>
                    
                <div class="form-group"><input type="submit" class="btn btn-primary" value="Submit" /></div>
            </form>
        </div>
    </div>
    <hr>
</div>

<aside class="col-lg-3 col-md-3 col-sm-3 blog-sidebar pull-right">
    <div class="sidebar-module">
        <h4>View</h4>
        <ol class="list-unstyled">
            <li><a href="/projects/"><i class="fas fa-briefcase"></i> My Projects</a></li>
        </ol>
    </div>

    <!--
    <div class="sidebar-module">
        <h4>Members</h4>
        <ol class="list-unstyled">
            <li><a href=""></a></li>
        </ol>
    </div>
    -->
</aside>
@endsection