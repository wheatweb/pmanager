@extends('layouts.app')

@section('content')
<div class="col-lg-9 col-md-9 col-sm-9 pull-left">
    <div class="jumbotron">
        <h1 class="display-3">{{ $project->name }}</h1>
        <p>Project for: {{ $company->name }}</p>
        <p>Time to complete: {{ $project->days }} days.</p>
        <p>{{ $project->description }}</p>
        <!-- <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more »</a></p> -->
    </div>
        
    <!-- Comments -->
    @include('partials.comments');
    
    <div class="row container-fluid">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <form method="post" action="{{ route('comments.store') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="comment-body">Comment</label>
                    <textarea placeholder="Enter Comments" id="comment-body" name="body" rows="3" spellcheck="false" class="form-control autosize-target text-left">
                    </textarea>
                </div>
                    
                <div class="form-group">
                    <label for="comment-url">URL to work done</label>
                    <input placeholder="Enter URL" id="comment-url" required name="url" spellcheck="false" class="form-control" />
                </div>
                    
                <input type="hidden" name="commentable_id" value="{{ $project->id }}" />
                <input type="hidden" name="commentable_type" value="App\Project" />
                <div class="form-group"><input type="submit" class="btn btn-primary" value="Submit" /></div>
            </form>
        </div>
    </div>
</div>
        
<aside class="col-lg-3 col-md-3 col-sm-3 pull-right">
    <div class="sidebar-module">
        @include('partials.view-nav')
        <h4>Actions</h4>
        <ol class="list-unstyled">
            <li><a href="/projects/{{ $project->id }}/edit/"><i class="fas fa-edit"></i> Edit {{ $project->name }}</a></li>
            <br />
            @if($project->user_id == Auth::user()->id)
            <li><a href="#" onclick="
                                    var result = confirm('Are you sure you wish to delete {{ $project->name }}?');
                                    if(result) {
                                        event.preventDefault();
                                        document.getElementById('delete-form').submit();
                                    }
                                    ">
                    <i class="fas fa-trash-alt"></i> Delete {{ $project->name }} Task
                </a>
                <form id="delete-form" action="{{ route('projects.destroy', [$project->id]) }}" method="POST" style="display: none;">
                    <input type="hidden" name="_method" value="delete" />
                    {{ csrf_field() }}
                </form>
            </li>
            @endif
        </ol>
        
        <h4>Team Members</h4>
        <ul class="list-unstyled" id="member-list">
            @foreach($project->users as $user)
            <li><a href="#">{{ $user->name }}</a></li>
            @endforeach
        </ul>
    </div>
        
    <!--
    <div class="sidebar-module">
        <h4>Members</h4>
        <ol class="list-unstyled">
            <li><a href=""></a></li>
        </ol>
    </div>
    -->
</aside>
<hr>
@endsection