@if(isset($errors)&&count($errors) > 0)
    <div class="alert alter-dismissable alert-danger fade show">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <ul>
        @foreach($errors->all() as $error)
            <li><strong>{!! $error !!}</strong></li>
        @endforeach
        </ul>
    </div>
@endif