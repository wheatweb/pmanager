    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <div class="card card-default container-fluid">
                <div class="card-heading">
                    <h3 class="card-title"><span class="glyphicon glyphicon-comment"></span>Recent Comments</h3>
                </div>
                <div class="card-body">
                    <ul class="media-list">
                        @foreach($comments as $comment)
                        <li class="media">
                            <div class="media-body">
                                <h4 class="media-heading">
                                    {{ $comment->user->name }}
                                    <br />
                                    <small>commented on: {{ $comment->created_at }}</small>
                                </h4>
                                <p>{{ $comment->body }}</p>
                                <b>URL:</b>
                                <p>{{ $comment->url }}</p>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>            
        </div>
    </div>