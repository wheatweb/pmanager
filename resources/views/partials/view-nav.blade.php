        <h4>View</h4>
        <ol class="list-unstyled">
            <li><a href="/companies/"><i class="fas fa-building"></i> My Companies</a></li>
            <li><a href="/projects/"><i class="fas fa-briefcase"></i> My Projects</a></li>
            <li><a href="/tasks/"><i class="fas fa-tasks"></i> My Tasks</a></li>
            <li><a href="/roles/"> Roles</a></li>
        </ol>
