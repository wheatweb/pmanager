@extends('layouts.app')

@section('content')
<div class="col-lg-12 col-md-12 col-sm-12 pull-left">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <h2>Your Session Expired</h2>
            <p>Please login in again to renew your session.</p>
            <p><a href="/login/">Login</a></p>
        </div>
    </div>
    <hr>
</div>
@endsection