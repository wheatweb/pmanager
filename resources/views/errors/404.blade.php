@extends('layouts.app')

@section('content')
<div class="col-lg-12 col-md-12 col-sm-12 pull-left">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <h2>Page Not Found</h2>
            <p>Opps, page was not found.</p>
            <p><a href="/">Home</a></p>
        </div>
    </div>
    <hr>
</div>
@endsection